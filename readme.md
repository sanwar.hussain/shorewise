# Proficiency Assignment for SHOREWISE

Both server and client are included in this repo. Please navigate to respective folders to run them.

NOTE: The mongodb instance key is valid for a week. 

## Installation

open client and server folders respectively, and run `yarn` It should add all the dependencies automatically.


## Usage

In the client folder, start the frontend app with the following

```javascript
yarn start

```

In the Server folder, start the backend app with the following

```javascript
yarn devstart

```

## Considerations

This app has been built on the MERN stack with limited time. The objective of this project is to demonstrate proficiency in the tech-stack. Design and organization efforts have been minimized. 

## License

[MIT](https://choosealicense.com/licenses/mit/)