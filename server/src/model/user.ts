import jwt from 'jsonwebtoken'
import mongoose from 'mongoose'

const userSchema = new mongoose.Schema({
	email: String,
	password: String,
	isAdmin: {
		type: Boolean,
		default: false,
	},
})

userSchema.methods.generateAuthToken = function () {
	// @ts-ignore
	return jwt.sign({ _id: this._id }, 'private123', { expiresIn: '1d' })
}

export default mongoose.model('testuser', userSchema)
