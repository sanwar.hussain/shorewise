import bcrypt from 'bcrypt'
import { NextFunction, Request, Response } from 'express'
import User from '../model/user'

export async function signupUser(
	req: Request,
	res: Response,
	next: NextFunction
) {
	const { email, password, role } = req.body
	console.log(req.body)
	if (!email || !password || !role)
		return res.status(400).send({ msg: 'bad content' })

	const salt = await bcrypt.genSalt(10)

	const hashedPassword: string = await bcrypt.hash(password, salt)

	const isAdmin = role.toLowerCase() === 'admin' ? true : false
	let user = await User.findOne({
		email,
		isAdmin,
	})

	if (user)
		return res
			.status(409)
			.send({ msg: 'User already registered, Please login' })

	req.body.password = hashedPassword
	user = await User.create({ ...req.body, isAdmin })

	//@ts-ignore
	const token = user.generateAuthToken()

	let newUser = user.toJSON()
	delete user.password
	//@ts-ignore
	newUser.token = token

	return res.status(200).send(newUser)
}

export async function signinUser(
	req: Request,
	res: Response,
	next: NextFunction
) {
	const { email, password, isAdmin } = req.body
	const user = await User.findOne({ email, isAdmin })

	if (!user) {
		return res.status(400).send({ msg: 'User not registered' })
	}

	if (!user.password)
		return res.status(400).send({ msg: 'Password not found!' })

	const isValidPass = await bcrypt.compare(password, user.password)

	if (!isValidPass) {
		return res.status(401).send({ msg: 'Invalid credentials!' })
	}

	//@ts-ignore
	let token = user.generateAuthToken()
	delete user.password

	const sanitizedUserInfo = {
		_id: user._id,
		token,
		isAdmin: user.isAdmin,
		email: user.email,
	}

	return res.status(200).send(sanitizedUserInfo)
}
