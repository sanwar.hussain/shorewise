import { useEffect, useState } from 'react'
import './App.css'

function App() {
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [role, setRole] = useState('')
	const [isAdmin, setIsAdmin] = useState(false)

	const [showSignup, setShowSignup] = useState(true)
	const [errorMsg, setErrorMsg] = useState('')
	const [user, setUser] = useState(null)

	function handleSignup() {
		console.log(email, password, role)

		if (!email || !password || !role) {
			return setErrorMsg('Please add all values!')
		}

		const url = 'http://localhost:5000/signup'
		fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email,
				password,
				role,
			}),
		})
			.then((response) => response.json())
			.then((response) => {
				console.log(response)
				//@ts-ignore
				if (!response.email) throw new Error('ERROR: ' + response.msg)

				//@ts-ignore
				setUser(response)
			})
			.catch((err) => {
				setErrorMsg(err.message)
			})
	}

	function handleSignin() {
		console.log(email, password, isAdmin)
		if (!email || !password) {
			setErrorMsg('Please add all values!')
		}

		const url = 'http://localhost:5000/signin'
		fetch(url, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
			},
			body: JSON.stringify({
				email,
				password,
				isAdmin,
			}),
		})
			.then((response) => response.json())
			.then((response) => {
				console.log(response)
				//@ts-ignore
				if (!response.email) throw new Error('ERROR: ' + response.msg)

				//@ts-ignore
				setUser(response)
				localStorage.setItem('user', JSON.stringify(response))
			})
			.catch((err) => {
				setErrorMsg(err.message)
			})
	}

	function handleLogout() {
		setUser(null)
		setErrorMsg('')
		localStorage.removeItem('user')
	}

	useEffect(() => {
		// @ts-ignore
		setUser(JSON.parse(localStorage.getItem('user')))
	}, [])

	return (
		<div className='master-container'>
			{user ? (
				<div className='dash-container'>
					{`Welcome, ${
						// @ts-ignore
						user.isAdmin ? 'Admin' : 'User'
					}`}
					<div className='logout-container'>
						<button className='btn-action logout' onClick={handleLogout}>
							Logout
						</button>
					</div>
				</div>
			) : (
				<div className='auth-container'>
					<header className='navbar-container'>
						<button
							className={`btn-action signin`}
							onClick={() => {
								setErrorMsg('')
								setShowSignup(false)
							}}
						>
							Signin
						</button>
						<button
							className={`btn-action signup`}
							onClick={() => {
								setErrorMsg('')
								setShowSignup(true)
							}}
						>
							Signup
						</button>
					</header>
					<div className='page-content'>
						{showSignup ? (
							<div className='content signup-content'>
								<input
									type='text'
									className='text-input'
									onChange={(e) => setEmail(e.target.value)}
								/>
								<input
									type='password'
									className='text-input'
									onChange={(e) => setPassword(e.target.value)}
								/>
								<div className='role-container'>
									<div className='input-container'>
										<input
											type='radio'
											name='role'
											value='admin'
											className='role-check'
											onChange={(e) => setRole(e.target.value)}
										/>
										Admin
									</div>
									<div className='input-container'>
										<input
											type='radio'
											name='role'
											value='user'
											className='role-check'
											onChange={(e) => setRole(e.target.value)}
										/>
										User
									</div>
								</div>
								<button className='btn-action' onClick={handleSignup}>
									Signup
								</button>
							</div>
						) : (
							<div className='content signin-content'>
								<input
									type='text'
									className='text-input'
									onChange={(e) => setEmail(e.target.value)}
								/>
								<input
									type='password'
									className='text-input'
									onChange={(e) => setPassword(e.target.value)}
								/>
								<div className='signup-admin-check'>
									<input
										type='checkbox'
										className='text-input'
										defaultChecked={isAdmin}
										onChange={(e) => setIsAdmin(!isAdmin)}
									/>
									<span>Admin</span>
								</div>

								<button className='btn-action' onClick={handleSignin}>
									Signin
								</button>
							</div>
						)}
					</div>
					{errorMsg ? <span className='msg-error'>{errorMsg}</span> : null}
				</div>
			)}
		</div>
	)
}

export default App
